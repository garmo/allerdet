#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
----------------------------------------------------------
UNIVERSIDAD DE SEVILLA
ESCUELA TÉCNICA SUPERIOR DE INGENIERÍA INFORMÁTICA
GRADO INGENIERÍA INFORMÁTICA - TECNOLOGÍAS INFORMÁTICAS

Trabajo fin de grado: Predicción de la alergenicidad potencial
de las proteínas alimentariasmediante diferentes técnicas de
Aprendizaje Automático.

Authors: Garcia-Moreno, Francisco M. and Gutiérrez-Naranjo, Miguel A.
Fecha: 30-06-2016 (ultima modificacion: 21/05/2017)
----------------------------------------------------------
"""

from collections import Counter
try:
    from .predict import predict
    from .predict import tuning_model
    from .predict import show_learning_methods_performance
    from .predict import tuning_model_performance
    from .predict import prediction_test
    from .predict import tpr_prod_tnr_score
    from .alignment import splitFastaSeqs
    from .alignment import how_many_seqs_from_a_are_duplicated_in_b
    from .alignment import create_fasta_file_without_duplications
    from .alignment import create_alignments_files
    from .alignment import secuences_by_ids
    from .preprocessing import features_for_extracting_to_string
except ImportError:
    from predict import predict
    from predict import tuning_model
    from predict import show_learning_methods_performance
    from predict import tpr_prod_tnr_score
    from alignment import splitFastaSeqs
    from alignment import how_many_seqs_from_a_are_duplicated_in_b
    from alignment import create_fasta_file_without_duplications
    from alignment import create_alignments_files
    from alignment import secuences_by_ids
    from preprocessing import features_for_extracting_to_string
    from predict import tuning_model_performance
    from predict import prediction_test

from sklearn.metrics import make_scorer
from sklearn.metrics import accuracy_score
from sklearn.metrics import recall_score
from sklearn.metrics import f1_score

if __name__ == '__main__':

    ##----------------------------------------------------------
    ## Predicciones
    ##----------------------------------------------------------

    # cp, pt = predict(method="rbm", params={'rbm__n_iter': 20, 'rbm__n_components': 1000, 'rbm__learning_rate': 0.001
    #         , "mod": "dt"
    #         , "mod_par": {'criterion': 'gini', 'max_depth': 5, 'min_samples_leaf': 3}}, m=1
    #                  , showAllPredictions=False, featToExtract=[True, True, True], webApp=False
    #                  ,testAlFile="a_cdpn.txt"
    #                  , testClass=0
    #                  , plotPosAlgn=True, plotNegAlgn=True, plotAIO=True)
    # counter = Counter(cp)
    # print(str(counter))
    # # print("Accuracy: " + str((counter['allergen'] * 100) / len(cp)))
    # print("Accuracy: " + str((counter['non-allergen'] * 100) / len(cp)))


    

    ##----------------------------------------------------------
    ## Performance
    ##----------------------------------------------------------
    # tuning_model_performance("dt", verbose=True)
    # tuning_model_performance("nb", verbose=True)
    # tuning_model_performance("knn", maxM=1, verbose=True)
    # tuning_model_performance("mlp", maxM=1, verbose=True)
    tuning_model_performance("rbm", maxM=1, kFolds=3, verbose=True, score={'tpr_prod_tnr':make_scorer(tpr_prod_tnr_score), 'accuracy': make_scorer(accuracy_score)}, refit="tpr_prod_tnr") #, posAlFile=oldAllergensAlgn, negAlFile=oldNonAllergensAlg, testPosAlFile=oldtestAllergensAlgn, testNegAlFile=oldtestNonAllergensAlg)




    # prediction_test("rbm", {'rbm__n_iter': 20, 'rbm__n_components': 1000, 'rbm__learning_rate': 0.001
    #         , "mod": "dt"
    #         , "mod_par": {'criterion': 'gini', 'max_depth': 5, 'min_samples_leaf': 3}}, 2, [True, True, True])


























