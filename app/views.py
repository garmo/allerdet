#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
----------------------------------------------------------
UNIVERSIDAD DE SEVILLA
ESCUELA TÉCNICA SUPERIOR DE INGENIERÍA INFORMÁTICA
GRADO INGENIERÍA INFORMÁTICA - TECNOLOGÍAS INFORMÁTICAS

Trabajo fin de grado: Predicción de la alergenicidad potencial
de las proteínas alimentariasmediante diferentes técnicas de
Aprendizaje Automático.

Authors: Garcia-Moreno, Francisco M. and Gutiérrez-Naranjo, Miguel A.
Fecha: 30-06-2016 (ultima modificacion: 27/04/2017)
----------------------------------------------------------
"""

from markupsafe import Markup
from flask import render_template, flash, request
from app import app

try:
    from .alignment import create_alignments_files, splitFastaSeqs, how_many_seqs_from_a_are_duplicated_in_b, filter_fasta_file,parse_info_protein
    from .predict import predict
    from .forms import AminoacidSequencesForm
except ImportError:
    from alignment import create_alignments_files, splitFastaSeqs, how_many_seqs_from_a_are_duplicated_in_b, filter_fasta_file,parse_info_protein
    from predict import predict
    from forms import AminoacidSequencesForm

# @app.errorhandler(404)
# def page_not_found(e):
#     return e

@app.route('/', methods=['GET','POST'])
# @app.route('/index', methods=['GET','POST'])
def index():
    myAllergenDataset = 'app/alignments/allergens_data_set.fasta'
    myNonAllergenDataset = 'app/alignments/nonallergens_data_set.fasta'

    form = AminoacidSequencesForm()
    if request.method == "POST" and form.validate_on_submit():
       # Guardar las secuencias del formulario en un archivo temporal
        temp_input_file = 'temp_input.fasta'
        with open(temp_input_file, 'w') as f:
            f.write(form.sequences.data)
        
        print("temp_input_file: ", temp_input_file)

        # Buscar duplicados en los datasets de alérgenos y no alérgenos
        duplicated_allergens = how_many_seqs_from_a_are_duplicated_in_b(temp_input_file, myAllergenDataset)
        duplicated_non_allergens = how_many_seqs_from_a_are_duplicated_in_b(temp_input_file, myNonAllergenDataset)
        print("cuantas secuencias están en mi dataset de allergens : " + str(len(duplicated_allergens)))
        print("cuantas secuencias están en mi dataset de non-allergens : " + str(len(duplicated_non_allergens)))

        # Obtener las IDs de las secuencias duplicadas
        duplicated_sequences = duplicated_allergens + duplicated_non_allergens
        duplicated_ids = {seq[0] for seq in duplicated_sequences}
        print("duplicated_ids: ", duplicated_ids)
        
        # Filtrar las secuencias duplicadas de las secuencias de entrada
        remaining_sequences = filter_fasta_file(temp_input_file, duplicated_ids)
        print("total remaining_sequences: ", len(remaining_sequences))

        # print("remaining_sequences: ", remaining_sequences)

        # Comprobar si hay secuencias restantes
        if len(remaining_sequences) > 0: 
            # Escribir las secuencias restantes a un archivo temporal
            temp_filtered_file = 'app/alignments/temp_web_filtered.fasta'
            with open(temp_filtered_file, 'w') as f:
                for seq_id, seq in remaining_sequences:
                    f.write(f">{seq_id}\n{seq}\n")
            print("temp_filtered_file: ", temp_filtered_file)
            temp_filtered_file = temp_filtered_file.split("/")[-1]

            predictions, protInfo, _, _, _, _, _, _, _, _, _ = predict(webApp=True,
                                                                    testSecFile=temp_filtered_file,#form.sequences.data,
                                                                    featToExtract=[True, False, False, False, False,
                                                                                    False, False, True], method="dt",
                                                                                    showAllPredictions=True)
            print("prediction:", predictions)
            print("protInfo", protInfo)
            for i in range(len(protInfo)):
                print(f"protInfo-{i}: {protInfo[i]}")
        else:
            predictions = []
            protInfo = []

        # Añadir las predicciones de las secuencias duplicadas
        for sid, seq in duplicated_sequences:
            if seq in [s for _, s in duplicated_allergens]:
                predictions.append(1)
            else:
                predictions.append(0)
            protInfo.append(parse_info_protein(sid))

        return render_template('result.html',
                               allPred=predictions,
                               protInfo=protInfo)
    else:
        return render_template('index.html',
                           form=form)

@app.route('/train-allergens')
def allergen_data():
    dataset=getDataSet(True, True)
    return render_template('dataset.html',data=dataset, isAllergen=True, isTrain=True)

@app.route('/test-allergens')
def allergen_test_data():
    dataset=getDataSet(True, False)
    return render_template('dataset.html',data=dataset, isAllergen=True, isTrain=False)

@app.route('/train-non-allergens')
def nonallergen_data():
    dataset=getDataSet(False, True)
    return render_template('dataset.html', data=dataset, isAllergen=False, isTrain=True)

@app.route('/test-non-allergens')
def nonallergen_test_data():
    dataset=getDataSet(False, False)
    return render_template('dataset.html', data=dataset, isAllergen=False, isTrain=False)

def getDataSet(allergen=True, train=True):
    dataset=""
    filename="app/alignments/"

    if allergen and train:
        filename+="allergens_data_set.fasta"
    elif not allergen and train:
        filename+="nonallergens_data_set.fasta"
    elif allergen and not train:
        filename+="test_allergens_data_set.fasta"
    else:
        filename+="test_nonallergens_data_set.fasta"
    
    lines=open(filename, "r").readlines()
    print(len(lines))
    for l in lines:
        dataset += Markup.escape(l) + Markup('<br />');
    return dataset

    
    


