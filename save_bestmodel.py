#save best model
from app.preprocessing import save_best_model
save_best_model(method="dt", params={'criterion': 'gini', 'max_depth': 5, 'min_samples_leaf': 1})

# save_best_model(method="rmb", params={'rbm__n_iter': 20, 'rbm__n_components': 50, 'rbm__learning_rate': 0.1, "mod": "dt", "mod_par": {'criterion': 'gini', 'max_depth': 5, 'min_samples_leaf': 1}})