from collections import Counter
import numpy as np

try:
    from app.predict import predict
    from app.predict import prediction_test
    from app.predict import tuning_model
    from app.predict import tuning_model_performance
    from app.predict import show_learning_methods_performance
    from app.predict import tpr_score
    from app.predict import tpr_prod_tnr_score
    from app.alignment import splitFastaSeqs
    from app.alignment import how_many_seqs_from_a_are_duplicated_in_b
    from app.alignment import create_fasta_file_without_duplications
    from app.alignment import create_alignments_files
    from app.alignment import secuences_by_ids
except SystemError:
    from predict import predict
    from predict import tuning_model
    from predict import show_learning_methods_performance
    from alignment import splitFastaSeqs
    from alignment import how_many_seqs_from_a_are_duplicated_in_b
    from alignment import create_fasta_file_without_duplications
    from alignment import create_alignments_files
    from alignment import secuences_by_ids


#Pruebas de predicción de alérgenos
print("Prueba Alérgenos DT")
cp,pt,ac,se,sp,ppv,f1Score,mcc,tpr,tnr,tpr_prod_tnr = predict(method="dt"
        , params={'criterion': 'gini', 'max_depth': 5, 'min_samples_leaf': 1}
        # ,params={'rbm__n_iter': 20, 'rbm__n_components': 50, 'rbm__learning_rate': 0.1, "mod": "dt" , "mod_par": {'criterion': 'gini', 'max_depth': 5, 'min_samples_leaf': 1}}
                , m=1, featToExtract=[True, False, False, False, False, False, False, True]
                # , showAllPredictions=True
                , webApp=False
                , printNativeClassReport=True
                , testNegAlFile="a_cdpn.txt"#oldtestNonAllergensAlg
                , testAlFile="a_cdp.txt")#"a_cdp_review.txt") #oldtestAllergensAlgn)#"a_cdp_review.txt")
counter = Counter(cp)
print(str(counter))
# print("Accuracy: " + str((counter[1]*100)/len(cp)))
print("accuracy: ", ac, "sensibility:", se, "specificity:", sp)

print("Prueba Alérgenos DT")
cp,pt,ac,se,sp,ppv,f1Score,mcc,tpr,tnr,tpr_prod_tnr = predict(method="dt"
        , params={'criterion': 'gini', 'max_depth': 5, 'min_samples_leaf': 1}
        # ,params={'rbm__n_iter': 20, 'rbm__n_components': 50, 'rbm__learning_rate': 0.1, "mod": "dt", "mod_par": {'criterion': 'gini', 'max_depth': 5, 'min_samples_leaf': 1}}
                , m=1, featToExtract=[True, False, False, False, False, False, False, True]
                # , showAllPredictions=True
                , webApp=False
                , printNativeClassReport=True
                , testNegAlFile=""#oldtestNonAllergensAlg
                , testAlFile="a_cdp_review.txt")#"a_cdp_review.txt") #oldtestAllergensAlgn)#"a_cdp_review.txt")

counter = Counter(cp)
print(str(counter))
print("Accuracy: " + str((counter[1]*100)/len(cp)))
print("accuracy: ", ac, "sensibility:", se, "specificity:", sp)





print("Prueba Alérgenos DT")
cp,pt,ac,se,sp,ppv,f1Score,mcc,tpr,tnr,tpr_prod_tnr = predict(method="dt"
        , params={'criterion': 'entropy', 'max_depth': 30, 'min_samples_leaf': 10}
                , m=1, featToExtract=[False, False, False, False, False, False, True, True]
                # , showAllPredictions=True
                , webApp=False
                , printNativeClassReport=True
                , testNegAlFile="a_cden.txt"#oldtestNonAllergensAlg
                , testAlFile="a_cdep.txt")#"a_cdp_review.txt") #oldtestAllergensAlgn)#"a_cdp_review.txt")
counter = Counter(cp)
print(str(counter))
print("Accuracy: " + str((counter[1]*100)/len(cp)))
print("accuracy: ", ac, "sensibility:", se, "specificity:", sp)



create_alignments_files(aligPos=False, aligNeg=False, aligTest=True, testSecFile='test_nonallergens_data_set.fasta', testAlFile="a_cdpn.txt", verbose=True)

