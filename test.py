#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
----------------------------------------------------------
UNIVERSIDAD DE SEVILLA
ESCUELA TÉCNICA SUPERIOR DE INGENIERÍA INFORMÁTICA
GRADO INGENIERÍA INFORMÁTICA - TECNOLOGÍAS INFORMÁTICAS

Trabajo fin de grado: Predicción de la alergenicidad potencial
de las proteínas alimentariasmediante diferentes técnicas de
Aprendizaje Automático.

Autor: Francisco Manuel García Moreno
Tutor: Miguel Ángel Gutiérrez Naranjo
Fecha: 30-06-2016 (ultima modificacion: 25/07/2024)
----------------------------------------------------------
"""
from collections import Counter
import numpy as np

try:
    from app.predict import predict
    from app.predict import prediction_test
    from app.predict import tuning_model
    from app.predict import tuning_model_performance
    from app.predict import show_learning_methods_performance
    from app.predict import tpr_score
    from app.predict import tpr_prod_tnr_score
    from app.alignment import splitFastaSeqs
    from app.alignment import how_many_seqs_from_a_are_duplicated_in_b
    from app.alignment import create_fasta_file_without_duplications
    from app.alignment import create_alignments_files
    from app.alignment import secuences_by_ids
except ImportError:
    from predict import predict
    from predict import tuning_model
    from predict import show_learning_methods_performance
    from alignment import splitFastaSeqs
    from alignment import how_many_seqs_from_a_are_duplicated_in_b
    from alignment import create_fasta_file_without_duplications
    from alignment import create_alignments_files
    from alignment import secuences_by_ids


if __name__ == '__main__':
    ##----------------------------------------------------------------------------------------
    ## Performance predicciones
    ##----------------------------------------------------------------------------------------
    from sklearn.metrics import make_scorer
    from sklearn.metrics import accuracy_score
    from sklearn.metrics import recall_score
    from sklearn.metrics import f1_score

    tuning_model_performance("dt", maxM=1, kFolds=3, verbose=True, score={'tpr_prod_tnr':make_scorer(tpr_prod_tnr_score), 'accuracy': make_scorer(accuracy_score)}, refit="tpr_prod_tnr") #, posAlFile=oldAllergensAlgn, negAlFile=oldNonAllergensAlg, testPosAlFile=oldtestAllergensAlgn, testNegAlFile=oldtestNonAllergensAlg)
    tuning_model_performance("rbm", maxM=1, kFolds=3, verbose=True, score={'tpr_prod_tnr':make_scorer(tpr_prod_tnr_score), 'accuracy': make_scorer(accuracy_score)}, refit="tpr_prod_tnr") #, posAlFile=oldAllergensAlgn, negAlFile=oldNonAllergensAlg, testPosAlFile=oldtestAllergensAlgn, testNegAlFile=oldtestNonAllergensAlg)


    #save best model
    from app.preprocessing import save_best_model
    save_best_model()

    from app.predict import predict
    predictions, protInfo, _, _, _, _, _, _, _, _, _ = predict(webApp=True, featToExtract=[True, False, False, False, False, False, False, True], method="rbm")


    tuning_model_performance("nb", maxM=1, verbose=True)
    tuning_model_performance("knn", maxM=1, verbose=True)
    tuning_model_performance("mlp", maxM=1, verbose=True)
    tuning_model_performance("rbm", maxM=1, reduction=500,  verbose=True, score={'tpr':make_scorer(tpr_score), 'accuracy': make_scorer(accuracy_score)}, refit='recall')

    cp,pt,ac,se,sp,ppv,f1Score,mcc,tpr,tnr,tpr_prod_tnr = predict(method="rbm", params={
        'rbm__n_iter': 20, 'rbm__n_components': 50, 'rbm__learning_rate': 0.1
        , "mod": "dt"
        , "mod_par": {'criterion': 'gini', 'max_depth': 5, 'min_samples_leaf': 1}}
                     , m=1
                     , showAllPredictions=False, featToExtract=[True, False, False, False, False, False, False, True], webApp=False
                     , testAlFile="a_cdp_allerpred.txt" #"a_cdp_review.txt" #"a_cdp_review2.txt"
                     , testNegAlFile="a_cdpn_allerpred.txt"
                     , printNativeClassReport=True
                     , plotPosAlgn=False
                     , plotNegAlgn=False
                     , plotAIO=False
                     , kFolds=0)

    d = how_many_seqs_from_a_are_duplicated_in_b("app/alignments/review/oleosin1_prunus_dulcis.fasta", myAllergenDataset)
    print("duplicaciones: " + str(len(d)))
    d = how_many_seqs_from_a_are_duplicated_in_b("app/alignments/review/oleosin1_arachis_hypogea.fasta", myAllergenDataset)
    print("duplicaciones: " + str(len(d)))

    #Pruebas de predicción de alérgenos
    print("Prueba Alérgenos DT")
    cp,pt,ac,se,sp,ppv,f1Score,mcc,tpr,tnr,tpr_prod_tnr = predict(method="dt"
          , params={'criterion': 'gini', 'max_depth': 5, 'min_samples_leaf': 1}
                    , m=1, featToExtract=[True, False, False, False, False, False, False, True]
                    # , showAllPredictions=True
                    , webApp=False
                    , printNativeClassReport=True
                    , testNegAlFile=""#oldtestNonAllergensAlg
                    , testAlFile="a_cdp_review.txt")#"a_cdp_review.txt") #oldtestAllergensAlgn)#"a_cdp_review.txt")


    print("Prueba Alérgenos DT")
    cp,pt,ac,se,sp,ppv,f1Score,mcc,tpr,tnr,tpr_prod_tnr = predict(method="dt"
          , params={'criterion': 'entropy', 'max_depth': 30, 'min_samples_leaf': 10}
                    , m=1, featToExtract=[False, False, False, False, False, False, True, True]
                    # , showAllPredictions=True
                    , webApp=False
                    , printNativeClassReport=True
                    , testNegAlFile=""#oldtestNonAllergensAlg
                    , testAlFile="")#"a_cdp_review.txt") #oldtestAllergensAlgn)#"a_cdp_review.txt")

    counter = Counter(cp)
    print(str(counter))
    print("Accuracy: " + str((counter[1]*100)/len(cp)))

    # print("Prueba Alérgenos RF")
    # cp,pt = predict(method="rf", params={}
    #                 , showAllPredictions=False, featToExtract=[True,True], webApp=False)
    # counter = Counter(cp)
    # print(str(counter))
    # print("Accuracy: " + str((counter['allergen']*100)/len(cp)))
    #
    # print("Prueba Alérgenos NB")
    # cp,pt = predict(method="nb", params={'priors': [0.5, 0.5]}
    #                 , showAllPredictions=False, featToExtract=[True,True], webApp=False)
    # counter = Counter(cp)
    # print(str(counter))
    # print("Accuracy: " + str((counter['allergen']*100)/len(cp)))
    #
    print("Prueba Alérgenos RBM")
    cp,pt = predict(method="rbm", params={'rbm__n_iter': 20, 'rbm__n_components': 1000, 'rbm__learning_rate': 0.001}
                    , showAllPredictions=False, featToExtract=[True,True, True, True], webApp=False)
    counter = Counter(cp)
    print(str(counter))
    print("Accuracy: " + str((counter['allergen']*100)/len(cp)))

    # print("Prueba Alérgenos MLP")
    # cp,pt = predict(method="mlp", params={'solver': 'lbfgs', 'max_iter': 300, 'hidden_layer_sizes': (100,)
    #     , 'activation': 'logistic'}, showAllPredictions=False, featToExtract=[True,True], webApp=False)
    # counter = Counter(cp)
    # print(str(counter))
    # print("Accuracy: " + str((counter['allergen']*100)/len(cp)))
    #
    # print("Prueba Alérgenos k-medias")
    # cp,pt = predict(method="km", params={'max_iter': 300, 'n_clusters': 2, 'algorithm': 'full'}
    #                 , showAllPredictions=False, featToExtract=[True,True], webApp=False)
    # counter = Counter(cp)
    # print(str(counter))
    # print("Accuracy: " + str((counter[0]*100)/len(cp)))
    #
    # # Pruebas de predicción de no alérgenos
    #
    print("Prueba NO Alérgenos kNN")
    cp, pt = predict(method="knn", params={'metric': 'euclidean', 'weights': 'uniform'
        , 'algorithm': 'auto', 'leaf_size': 1, 'p': 1, 'n_neighbors': 3}, showAllPredictions=False,
                     featToExtract=[True, True, True, True]
                     , webApp=False, plotPosAlgn=False, plotNegAlgn=False, plotTestAlgn=False, plotAIO=False,
                     figSameColor=False,testAlFile="a_cdpn.txt")
    counter = Counter(cp)
    print(str(counter))
    print("Accuracy: " + str((counter['non-allergen'] * 100) / len(cp)))

    # print("Prueba NO Alérgenos DT")
    # cp, pt = predict(method="dt", params={'criterion': 'gini', 'max_depth': 10, 'min_samples_leaf': 8}
    #                  , showAllPredictions=False, featToExtract=[True, True], webApp=False,testAlFile="a_cdpn.txt")
    # counter = Counter(cp)
    # print(str(counter))
    # print("Accuracy: " + str((counter['non-allergen'] * 100) / len(cp)))
    #
    # print("Prueba NO Alérgenos RF")
    # cp, pt = predict(method="rf", params={}
    #                  , showAllPredictions=False, featToExtract=[True, True], webApp=False,testAlFile="a_cdpn.txt")
    # counter = Counter(cp)
    # print(str(counter))
    # print("Accuracy: " + str((counter['non-allergen'] * 100) / len(cp)))
    #
    # print("Prueba NO Alérgenos NB")
    # cp, pt = predict(method="nb", params={'priors': [0.5, 0.5]}
    #                  , showAllPredictions=False, featToExtract=[True, True], webApp=False,testAlFile="a_cdpn.txt")
    # counter = Counter(cp)
    # print(str(counter))
    # print("Accuracy: " + str((counter['non-allergen'] * 100) / len(cp)))
    #
    print("Prueba NO Alérgenos RBM")
    cp, pt = predict(method="rbm", params={'rbm__n_iter': 20, 'rbm__n_components': 1000, 'rbm__learning_rate': 0.001}
                     , showAllPredictions=False, featToExtract=[True, True, True, True]
        , webApp=False,testAlFile="a_cdpn.txt")
    counter = Counter(cp)
    print(str(counter))
    print("Accuracy: " + str((counter['non-allergen'] * 100) / len(cp)))

    # print("Prueba NO Alérgenos MLP")
    # cp, pt = predict(method="mlp", params={'solver': 'lbfgs', 'max_iter': 300, 'hidden_layer_sizes': (100,)
    #     , 'activation': 'logistic'}, showAllPredictions=False, featToExtract=[True, True]
    #     , webApp=False,testAlFile="a_cdpn.txt")
    # counter = Counter(cp)
    # print(str(counter))
    # print("Accuracy: " + str((counter['non-allergen'] * 100) / len(cp)))
    #
    # print("Prueba NO Alérgenos k-medias")
    # cp, pt = predict(method="km", params={'max_iter': 300, 'n_clusters': 2, 'algorithm': 'full'}
    #                  , showAllPredictions=False, featToExtract=[True, True], webApp=False,testAlFile="a_cdpn.txt")
    # counter = Counter(cp)
    # print(str(counter))
    # print("Accuracy: " + str((counter[0] * 100) / len(cp)))



    # ids = [pt[i][1] for i, c in enumerate(cp) if c == 'non-allergen' and len(pt[i]) > 1]
    # print(len(ids))
    # # print(ids[:3])
    # # # # creacion del nuevo conjunto de test de alérgenos
    # exclusion = splitFastaSeqs(myAllergenDataset)[1]
    # ex=secuences_by_ids(myTestDataset, ids)
    # print(len(ex))
    # exclusion.extend(ex)
    # create_fasta_file_without_duplications([myTestDataset, myValidationDataset],myTestDataset, exclusion)


    # accuracy con cross validation k-fold=10 y estratificación

    show_learning_methods_performance([True,True, True, True], method="knn", params={'metric': 'euclidean', 'weights': 'uniform'
        , 'algorithm': 'auto', 'leaf_size': 1, 'p': 1, 'n_neighbors': 3}, kFolds=10)

    # show_learning_methods_performance([True,True], method="rf", params={}, kFolds=10)

    # show_learning_methods_performance([True,True], method="dt"
    #                                   , params={'criterion': 'gini', 'max_depth': 10, 'min_samples_leaf': 8}, kFolds=10)
    #
    # show_learning_methods_performance([True,True], method="nb", params={'priors': [0.5, 0.5]}, kFolds=10)
    #
    # show_learning_methods_performance([True,True], method="mlp", params={'solver': 'lbfgs', 'max_iter': 300
    #     , 'hidden_layer_sizes': (100,), 'activation': 'logistic'}, kFolds=10)
    # #
    show_learning_methods_performance([True,True, True, True], method="rbm", params={'rbm__n_iter': 20, 'rbm__n_components': 1000
        , 'rbm__learning_rate': 0.001}, kFolds=10)

































